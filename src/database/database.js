export const actions = [
  {
    id: 11,
    type: "metadata",
    actionTarget: 1,
    actionProps: [],
    reactionTarget: 2,
    reactionProps: [],
    embeddedAction: {},
    embeddedReaction: {}
  },
  {
    id: 1,
    type: "input",
    actionTarget: 1,
    actionProps: ["form"],
    reactionTarget: 2,
    reactionProps: [],
    embeddedAction: {
      form: []
    },
    embeddedReaction: {}
  },
  {
    id: 2,
    type: "model-confirmation",
    actionTarget: 1,
    actionProps: ["action-buttom", "data-model", "output", "delivery"],
    reactionTarget: 2,
    reactionProps: ["introduction-text", "action-buttom"],
    embeddedAction: {
      "action-buttom": "",
      "data-model": [],
      output: 1,
      delivery: 1
    },
    embeddedReaction: {
      "introduction-text": "",
      "action-buttom": ""
    }
  },
  {
    id: 3,
    type: "output",
    actionTarget: 1,
    actionProps: ["type", "output"],
    reactionTarget: 2,
    reactionProps: [],
    embeddedAction: {
      type: [],
      output: []
    },
    embeddedReaction: {}
  },
  {
    id: 4,
    type: "assign",
    actionTarget: 1,
    actionProps: ["role", "person", "message"],
    reactionTarget: 2,
    reactionProps: [],
    target: 1,
    embeddedAction: {
      role: [],
      person: [],
      message: []
    },
    embeddedReaction: {}
  },
  {
    id: 5,
    type: "hook",
    actionTarget: 1,
    actionProps: ["reference", "action"],
    reactionTarget: 2,
    reactionProps: [],
    target: 1,
    embeddedAction: {
      reference: [],
      action: []
    },
    embeddedReaction: {}
  }
];

export const components = [
  {
    tag: "input",
    type: "text",
    label: "Seu nome",
    value: "",
    title: "Campo de texto curto",
    placeholder: "",
    slug: "",
    validation: {
      required: false,
      type: "url", // url, email, int, float, phone...
      typeof: String,
      between: [],
      sameAs: "",
      "min-length": 0,
      "max-length": -1,
      regex: ""
    }
  },
  {
    tag: "input",
    type: "number",
    label: "",
    value: "",
    title: "Campo de numero",
    slug: "",
    validation: {
      required: false,
      type: "int", // url, email, int, float, phone...
      typeof: Number,
      between: [0, 20],
      sameAs: "",
      "min-length": 0,
      "max-length": -1,
      regex: ""
    }
  },
  {
    tag: "input",
    type: "email",
    label: "",
    value: "",
    title: "Campo de email",
    slug: "",
    validation: {
      required: false,
      type: "email", // url, email, int, float, phone...
      typeof: String,
      between: [],
      sameAs: "",
      "min-length": 0,
      "max-length": -1,
      regex: ""
    }
  }
];

export const targets = [
  {
    id: 1,
    type: "publisher"
  },
  {
    id: 2,
    type: "author"
  },
  {
    id: 3,
    type: "third-party"
  },
  {
    id: 4,
    type: "assigned"
  },
  {
    id: 5,
    type: "any"
  }
];
